#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));
	cout << "Enter size of dynamic array: ";
	int size;
	cin >> size;
	system("cls");
	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}


	while (true)
	{
		cout << "Unordered contents: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << " ";
		cout << "\nOrdered contents:   ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << " ";

		cout << "\n\nWhat do you what to do?";
		cout << "\n1 - Remove element at index";
		cout << "\n2 - Search for element ";
		cout << "\n3 - Expand and generate random values";
		cout << "\n";

		int choice;
		cin >> choice;
		
		if (choice == 1)
		{
			cout << "\n\nEnter index to remove: ";
			int Delete;
			cin >> Delete;

			unordered.remove(Delete);
			ordered.remove(Delete);

			cout << "\nElement removed: " << Delete;
			cout << "\nUnordered contents: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";
			cout << "\nOrdered contents:   ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " ";
			cout << "\n";
			system("pause");
			system("cls");
		}

		if (choice == 2)
		{
			cout << "\nInput element to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			cout << "\n";
			system("pause");
			system("cls");
		}

		if (choice == 3)
		{
			cout << "\n\nInput size of expansion: ";
			int expansion;
			cin >> expansion;

			for (int i = 0; i < expansion; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << "\nArrays have been expanded: ";
			cout << "\nUnordered contents: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";
			cout << "\nOrdered contents:   ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " ";
			cout << "\n";
			system("pause");
			system("cls");
		}
	}
	system("pause");
}
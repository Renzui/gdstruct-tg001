#include <iostream>
#include <string>

using namespace std;

int recursiveAddition(int from)
{
	if (from == 0)
		return 0;
	return (from % 10 + recursiveAddition(from / 10));
}

int fibonacciNumbers(int n)
{
	if ((n == 1) || (n == 0)) {
		return n;
	}
	else {
		return(fibonacciNumbers(n - 1) + fibonacciNumbers(n - 2));
	}
}

bool checkPrime(int x, int i = 2)
{
	if (x <= 2)
		return (x == 2) ? true : false;
	if (x % i == 0)
		return false;
	if (i * i > x)
		return true;

	return checkPrime(x, i + 1);
}

int main()
{
	int num;
	cout << "Put any numbers you want so it will add per number: ";
	cin >> num;

	int result = recursiveAddition(num);
	cout << "Sum of the digits in: " << num << " = " << result << endl;
	cout << "\n";

	int n;
	int i = 0;
	cout << "Enter the number of terms in series: ";
	cin >> n;
	cout << "Fibonacci numbers: ";
	while (i < n)
	{
		cout << " " << fibonacciNumbers(i);
		i++;
	}
	cout << "\n";

	int prime;
	cout << "\n";
	cout << "Enter any number to check if it's a prime number or not: ";
	cin >> prime;

	if (checkPrime(prime))
		cout << "Yes it's a prime number";
	else
		cout << "No it's not a prime number";

	cout << "\n";

	system("pause");
	return 0;
}
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

struct item
{
	string name;
	int value;
};

int getRandomNumber()
{
	int x = rand() % 10 + 1;
	return x;
}

string getRandomItem()
{
	string nameDatabase[20] =
	{
		"Berserker Greaves",
		"Cull",
		"Dead Man's Plate",
		"Infernal Mask",
		"Hunter's Machete",
		"Luden's Echo",
		"Nashor's Tooth",
		"Might of the Ruined King",
		"Ninja Tabi",
		"Obsidian Clever",
		"Ruby Crystal",
		"Thornmail",
		"Stopwatch",
		"Trinity Force",
		"Youmuu's Ghostblade",
		"Void Staff",
		"Quicksilver Sash",
		"Lich Bane",
		"Iceborn Gauntlet",
		"Edge of Night"
	};

	for (int i = 0; i < 10; i++)
	{
		string randomname = nameDatabase[rand() % 20];
		return randomname;
	}

}

int main()
{
	srand(time(NULL));
	
	item inventory[10];
	int sum = 0;

	for (int i = 0; i < 10; i++)
	{
		inventory[i].name = getRandomItem();
		cout << "Item: " << inventory[i].name << endl;
		inventory[i].value = getRandomNumber();
		cout << "Value: " << inventory[i].value << endl;
		sum = sum + inventory[i].value;
	}
	cout << "Total Value: " << sum << endl;
	

	system("pause");
	return 0;
}
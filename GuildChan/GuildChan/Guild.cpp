#include<iostream>
#include<string>

using namespace std;

int main()
{

	string guildName;
	cout << "Declare a name for the guild: ";
	cin >> guildName;

	system("pause");
	system("cls");

	int guildSize;
	cout << "Declare an initial size for the guild: ";
	cin >> guildSize;

	system("pause");
	system("cls");

	string *guildMembers = new string[guildSize];

	for (int i = 0; i < guildSize; i++)
	{
		cout << "Enter username for " << guildName << " Member #" << i + 1 << " : ";
		cin >> guildMembers[i];
	}

	system("pause");
	system("cls");

	for (int i = 0; i < guildSize; i++)
	{
		cout << guildName << " Member #" << "[" << i + 1 << "]" << " :" << guildMembers[i] << endl;
	}

	cout << endl;
	system("cls");

	while (true)
	{
		int guildChoice;

		cout << guildName << endl << endl;

		for (int i = 0; i < guildSize; i++)
		{
			cout << guildName << " Member #" << "[" << i + 1 << "]" << " :" << guildMembers[i] << endl;
		}
		cout << endl;

		cout << "What would you like to choose?: " << endl;
		cout << " [1] Print every member " << endl;
		cout << " [2] Renaming a member " << endl;
		cout << " [3] Adding a member " << endl;
		cout << " [4] Deleting a member " << endl;
		cout << endl;
		cin >> guildChoice;

		//Printing member
		if (guildChoice == 1)
		{
			cout << "Print every member: " << endl;

			for (int i = 0; i < guildSize; i++)
			{
				cout << " #" << i + 1 << " " << guildMembers[i] << endl;
			}
			cout << endl;
			system("pause");
			system("cls");
		}
		//Renaming a member
		else if (guildChoice == 2)
		{
			string rename;
			cout << "Type the name of the member who you want to rename: " << endl;
			cin >> rename;

			for (int i = 0; i < guildSize; i++)
			{
				if (rename == guildMembers[i])
				{
					cout << "Rename: " << endl;
					cin >> guildMembers[i];
					cout << "Member renamed to: " << guildMembers[i] << endl;
				}
			}
			cout << endl;
			system("pause");
			system("cls");
		}
		//Adding member
		else if (guildChoice == 3)
		{
			cout << "Adding a member: " << endl;
		}
		//Deleting member
		else if (guildChoice == 4)
		{
			string Delete;
			cout << "Deleting a member: " << endl;
			cin >> Delete;

			int count = 0;
			for (int i = 0; i < guildSize; i++)
			{
				if (Delete == guildMembers[i])
				{
					for (int a = i; a < (guildSize - 1); a++)
					{
						guildMembers[a] = guildMembers[i + 1];
					}
					count++;
					break;
				}
			}
			if (count == 0)
			{
				cout << guildName << " Member deleted " << endl;
				for (int i = 0; i < guildSize - 1; i++)
				{
					cout << "[" << i + 1 << "]" << guildMembers[i] << endl;
				}
				guildSize--;
			}
			system("pause");
		}
	}
	system("pause");
	return 0;
}
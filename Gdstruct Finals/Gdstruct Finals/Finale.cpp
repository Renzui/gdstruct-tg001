#include <iostream>
#include "Queue.h"
#include "Stack.h"
#include "UnorderedArray.h"

using namespace std; 

int main()
{
	int setSize;
	cout << "Enter size of element sets: ";
	cin >> setSize;
	cout << "\n\n";
	Queue<int> queue(setSize);
	Stack<int> stack(setSize);
	while (true)
	{
		char choice = 0;
		cout << "What do you want to do?";
		cout << "\n1 - Push Elements";
		cout << "\n2 - Pop Elements";
		cout << "\n3 - Print Everything then empty set\n";
		cin >> choice;

		switch (choice)
		{
		case '1':
			int Push;
			cout << "\n\nEnter number: ";
			cin >> Push;
			queue.push(Push);
			stack.push(Push);
			cout << "\n\nTop element of sets: ";
			cout << "\nQueue: " << queue.top(Push);
			cout << "\nStack: " << stack.top(Push) <<"\n";
			system("pause");
			break;
		case '2':
			queue.remove(0);
			stack.pop();
			cout << "\n\nYou have popped the front elements.\n\n";
			cout << "Top elements of sets: \n";
			cout << "Queue: " << queue.top(Push) << "\n";
			cout << "Stack: " << stack.top(Push) << "\n";
			system("pause");
			break;
		case '3':
			cout << "Queue elements: \n";
			for (int i = 0; i < queue.getSize(); i++)
			{
				cout << queue[i] << "\n";
			}
			cout << "Stack elements: \n";
			for (int i = (stack.getSize() - 1); i >= 0; i--)
			{
				cout << stack[i] << "\n";
			}

			for (int i = 0; i < Push; i++)
			{
				queue.pop();
				stack.pop();
			}

			system("pause");
			break;
		}
		system("cls");
	}

	system("pause");
	return 0;
}